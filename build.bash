#!/usr/bin/env bash
rm -rf build
mkdir build

# Build Frontend
cd frontend/public
NODE_ENV=production npx tailwindcss-cli@latest build index.in.css -o index.out.css
cd ..
npm run build && npm run export
cd ..
mv frontend/out build/frontend

# Backend
cd backend
source venv/bin/activate
if [ ! -f links.db ]; then
  python3 setup_db.py
fi
cd ..
rsync -ax --exclude venv backend/ build/backend

cp README-deploy.md build
