#!/usr/bin/env bash

set -e

source ./common.sh
MAIN_PATH=$(pwd)

function setup_frontend() {
  prefix_stdout_stderr "${PURPLE}frontend: ${NC}"

  cd ./frontend

  echo "Installing dependencies..."
  npm i

  echo "Building tailwind css file for links page"
  cd public
  NODE_ENV=production npx tailwindcss-cli@latest build index.in.css -o index.out.css

  echo "Done!"
}

function setup_backend() {
  prefix_stdout_stderr "${CYAN}backend:  ${NC}"

  cd ./backend

  echo "Deleting old virtual environment..."
  rm -rf ./venv

  echo "Deleting sqlite database..."
  rm ./links.db || echo "Nothing to delete ¯\_(ツ)_/¯"

  echo "Deleting .env file"
  rm ./.env || echo "No .env to delete"

  echo "Creating new virtual environment..."
  python3 -m venv venv
  source venv/bin/activate

  echo "Installing dependencies..."
  pip install -r requirements.txt

  echo "Creating a dummy sqlite database at 'backend/links.db'..."
  python setup_db.py

  echo "Creating .env file"
  echo "PASSWORD=test" > .env
  echo "PORT=5000" >> .env
  echo "OUT_PATH=$MAIN_PATH/frontend/public/index.html" >> .env

  echo "Done!"
}

run_frontend_backend "setup_frontend" "setup_backend"
