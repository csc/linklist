// eslint-disable-next-line no-undef
module.exports = {
  purge: {
    content: [
      "./pages/**/*.{js,ts,jsx,tsx}",
      "./components/**/*.{js,ts,jsx,tsx}",
    ],
    // These options are passed through directly to PurgeCSS
    options: {
      keyframes: true,
      fontFace: true,
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      body: ["Karla", "sans-serif"],
    },
    extend: {
      colors: {
        analytics: {
          "view-icon": "#39e09b",
          "click-icon": "#8a86e5",
        },
        gray: {
          450: "#3d3b3c",
        },
      },
      fontFamily: {
        sans: ["Karla", "Helvetica", "Ubuntu", "sans-serif"],
        karla: ["Karla", "Verdana", "sans-serif"],
      },
      fontSize: {
        s: ".82rem",
      },
      screens: {
        "lt-lg": "992px",
      },
      flex: {
        analytics: "1 0 0",
        chevron: "0 0 16px",
      },
      borderRadius: {
        preview: "3.2rem",
      },
      borderWidth: {
        preview: "0.75rem",
      },
      minHeight: {
        link: "48px",
      },
    },
  },
  variants: {
    extend: {
      opacity: ["disabled"],
    },
  },
};
