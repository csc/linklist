# Links Backend

## Setup

1. Install Python 3.

2. Run the following commands inside the backend folder:
```
python3 -m venv venv
venv/Scripts/activate
pip install -r requirements.txt
python main.py
```

If you are on Windows, run `venv\Scripts\activate` instead, for the second command.

3. Go to `localhost:5000` in your web browser and you should see "Hello, World!".

## (Optional) Exercises

- Install Postman (https://www.postman.com/downloads/) and use it make a request to this web server.
- Add a new GET route at `/test` that shows "A new route".
- Add a new POST route at `/post` that accepts a JSON body, and shows the `foo` key of the JSON.
