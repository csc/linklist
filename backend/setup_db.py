import sqlite3
import os

DB_PATH = os.path.join(os.path.dirname(__file__), 'links.db')

con = sqlite3.connect(DB_PATH)

# array of links to store
links = [
  ('http://csclub.uwaterloo.ca/','CS Club Website',0,0,1),
  ('https://www.instagram.com/uwcsclub/','Instagram',0,1,1),
  ('https://www.facebook.com/uw.computerscienceclub','Facebook',0,2,1),
  ('http://twitch.tv/uwcsclub','Twitch',0,3,1),
  ('http://bit.ly/uwcsclub-yt','YouTube',0,4,1),
]

# SQLite setup
cur = con.cursor()

# test if table already exists
cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='links'")
if cur.fetchone():
  raise Exception('Links table already exists.')
else:
  cur.execute('''CREATE TABLE links (
    url text NOT NULL,
    name text NOT NULL,
    clicks int NOT NULL,
    position int NOT NULL UNIQUE,
    active int NOT NULL,
    UNIQUE(url, name)
    )''')
  cur.executemany('INSERT INTO links VALUES (?,?,?,?,?)', links)
  con.commit()

con.close()
