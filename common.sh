#!/usr/bin/env bash

PURPLE='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m'

function prefix_stdout_stderr() {
  exec > >(trap "" INT TERM; sed "s/^/`printf "$1"`/")
  exec 2> >(trap "" INT TERM; sed "s/^/`printf "$1"`/" >&2)
}

function run_frontend_backend() {
  $1 &
  pid_frontend=$!

  $2 &
  pid_backend=$!

  trap_ctrlc() {
    echo ""
    kill $pid_frontend
    echo "frontend exit code: $?"
    kill $pid_backend
    echo "backend exit code: $?"
  }

  trap trap_ctrlc INT

  wait
}